# Docker for Atlassian JIRA

[![](https://badge.imagelayers.io/goellnerit/jira-docker:latest.svg)](https://imagelayers.io/?images=goellnerit/jira-docker:latest 'Get your own badge on imagelayers.io')

Simply run Docker with:
    
	docker run -d -p 8080:8080 goellnerit/jira-docker
    
If you are using OSX you first need to find out your boot2docker IP before you can access your application:

	boot2docker ip
    
Now open http://[Docker Host IP]:8080 in your browser.

On a live environment, we are using nginx from jwilder:

https://hub.docker.com/r/jwilder/nginx-proxy/

So to run the application at your own url just run the jwilder/nginx-proxy and use the following command:

	docker run -d -VIRTUAL_HOST=subdomain.domain.tld -VIRTUAL_PORT=8080 goellnerit/jira-docker

If you need a persistant file system and mysql database, please use:

	#mysql server
	docker run --name yourmysql -v /data/mysql/conf:/etc/mysql/conf.d -v /data/mysql/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=[yourpasswordhere] -d mysql:latest

	#data container
	docker run -u root --name data-jira -v /dockerData/jiraHome:/var/local/atlassian/jira goellnerit/jira-docker chown -R daemon /var/local/atlassian/jira

	#persistant storage and connected database
	docker run -v /data/jira:/var/local/atlassian/jira --link yourmysql:db --d -e RAM_MIN=2048 -e RAM_MAX=4096 -e VIRTUAL_HOST=subdomain.yourdomain.tld goellnerit/jira-docker
	
If you want to connect to the server using remote debugging in your environment:

	docker run -p 8080:8080 -p 5005:5005 -e CONF_REMOTE_DEBUG=true -d -e RAM_MIN=2048 -e RAM_MAX=4096 goellnerit/jira-docker
	
# Docker Compose

An example to run it in a Docker compose file:


```
#!shell

jira:
  image: goellnerit/jira-docker
  volumes:
    - /data/[your directory]:/var/atlassian/jira
  links:
    - mysql:db
  environment:
    - TIME_ZONE=Europe/Berlin
    - VIRTUAL_HOST=[your url if you run with nginx]
    - VIRTUAL_PORT=8080
    - RAM_MIN=2048m
    - RAM_MAX=8048m
    - HTTPS=true
    - PROXY_NAME=[your url if you run with nginx will be added to server.xml]
    - NEWRELIC_LICENSE=xxxxxxxxxxxxxxxxxxxxxxxx
    - NEWRELIC_APP_NAME=[your name if you want to use newrelic! if not remove the 3 newrelic lines]
    - NEWRELIC=true
  restart: always

mysql:
  image: mysql:5.6
  volumes:
    - /data/mysql/conf:/etc/mysql/conf.d
    - /data/mysqljira:/var/lib/mysql
  environment:
    - MYSQL_ROOT_PASSWORD=[your very long passwort you need in case you need root to the db]
    - MYSQL_DATABASE=jira
    - MYSQL_USER=jira
    - MYSQL_PASSWORD=[your very long password]
  restart: always

```



# More Repositories
If you are searching for other Atlassian tools, feel free to checkout our other repositories at https://hub.docker.com/u/goellnerit/ and https://bitbucket.org/philippgoellner/

# Open Source Effort
If you want to help with our Docker effort, please send us a pull request or just shoot over an email with your Bitbucket account to get rights on this repository.

# Need Consultancy?
If you need help using this images or integrating them into your infrastructure / CI process etc., feel free to contact me at philipp.goellner@xaltbc.de
