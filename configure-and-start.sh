#!/bin/sh
env CATALINA_OPTS="-Xms${RAM_MIN}m -Xmx${RAM_MAX}m $CATALINA_OPTS"

# Enable remote debug
if [ "$JIRA_REMOTE_DEBUG" = "true" ]; then
	CATALINA_OPTS="-Xdebug -Xrunjdwp:transport=dt_socket,address=5005,server=y,suspend=n $CATALINA_OPTS"
fi

# Configure server.xml if application is behind reverse proxy
if [ "$PROXY_NAME" != "false" ] && [ ! -f ${JIRA_INSTALL}/PROXYNAME ]; then
	xmlstarlet ed -i /Server/Service/Connector -t attr -n proxyName -v ${PROXY_NAME} ${JIRA_INSTALL}/conf/server.xml > /tmp/generated-server.xml
	mv /tmp/generated-server.xml ${JIRA_INSTALL}/conf/server.xml
	touch ${JIRA_INSTALL}/PROXYNAME
fi

# Configure server.xml if application is behind SSL enabled reverse proxy
if [ "$HTTPS" = "true" ] && [ ! -f ${JIRA_INSTALL}/HTTPS ]; then
	xmlstarlet ed -i /Server/Service/Connector -t attr -n proxyPort -v 443 ${JIRA_INSTALL}/conf/server.xml > /tmp/generated-server.xml
	mv /tmp/generated-server.xml ${JIRA_INSTALL}/conf/server.xml
	xmlstarlet ed -i /Server/Service/Connector -t attr -n scheme -v https ${JIRA_INSTALL}/conf/server.xml > /tmp/generated-server.xml
	mv /tmp/generated-server.xml ${JIRA_INSTALL}/conf/server.xml
	touch ${JIRA_INSTALL}/HTTPS
fi

if [ "$IMPORTCERT" = "true" ]; then 
	CATALINA_OPTS="-Djavax.net.ssl.trustStore=/opt/jdk/current/jre/lib/security/cacerts -Djavax.net.ssl.trustStorePassword=changeit $CATALINA_OPTS"      

	cd ${IMPORTPATH}
	if [ ! -f ${JIRA_INSTALL}/skipcert.conf ]; then
		for i in *
		do 
			/usr/lib/jvm/java-8-oracle/jre/bin/keytool -keystore /usr/lib/jvm/java-8-oracle/jre/lib/security/cacerts -importcert -alias $i -file $i -storepass changeit -noprompt
		done
		touch ${JIRA_INSTALL}/skipcert.conf
	fi
	cd ${JIRA_INSTALL}
fi


# Install and configure New Relic
if [ "$NEWRELIC" = "true" ]; then
	unzip newrelic-java-${NEWRELIC_VERSION}.zip -d ${JIRA_INSTALL}

	cd ${JIRA_INSTALL}/newrelic
	java -jar newrelic.jar install
	sed -ri "s/app_name:.*$/app_name: ${NEWRELIC_APP_NAME}/" newrelic.yml
	sed -ri "s/license_key:[ \t]*'[^']+'/license_key: '${NEWRELIC_LICENSE}'/" newrelic.yml
fi

# Loop used to wait for a service to be ready which this application depends on
if [ "$WAIT" = "true" ]; then

        is_ready() {
                eval "$WAIT_COMMAND"
        }

        # wait until is ready
        i=0
        while ! is_ready; do
                i=`expr $i + 1`
                if [ $i -ge ${WAIT_LOOPS} ]; then
                        echo "$(date) - still not ready, giving up"
                        exit 1
                fi
        echo "WAITING: $(date) - waiting to be ready for ${WAIT_COMMAND}"
        sleep ${WAIT_SLEEP}
        done
fi

# Download and install AddOns
if [ "$INSTALL_PLUGINS" = "true" ] && [ ! -f ${JIRA_HOME}/plugins_installed ] ; then
	if [ $INSTALL_PLUGINS_URLS_V1 != "false" ]; then
		PLUGINS_URLS=$(echo $INSTALL_PLUGINS_URLS_V1 | tr ";" "\n")
		for PLUGIN_URL in $PLUGINS_URLS
		do
			PLUGIN_ID=$(echo $PLUGIN_URL | sed -e "s/.*\/\(.*\)$/\1/")
			wget --content-disposition -o ${JIRA_HOME}/plugins_install_v1_${PLUGIN_ID}.log -P ${JIRA_INSTALL}/atlassian-jira/WEB-INF/lib/ $PLUGIN_URL
		done
	fi
	if [ $INSTALL_PLUGINS_URLS_V2 != "false" ]; then
		PLUGINS_URLS=$(echo $INSTALL_PLUGINS_URLS_V2 | tr ";" "\n")
		for PLUGIN_URL in $PLUGINS_URLS
		do
			PLUGIN_ID=$(echo $PLUGIN_URL | sed -e "s/.*\/\(.*\)$/\1/")
#			wget --content-disposition -o ${JIRA_HOME}/plugins_install_v2_${PLUGIN_ID}.log -P ${JIRA_HOME}/plugins/installed-plugins/ $PLUGIN_URL
			wget --content-disposition -o ${JIRA_HOME}/plugins_install_v2_${PLUGIN_ID}.log -P /tmp/plugins $PLUGIN_URL
		done
		if [ -d /tmp/plugins ]; then
			rm -rf /tmp/plugins
		fi
		mkdir /tmp/plugins
		# Note: obr Add-ons normally cannot be installed in this way; the only
		# approved way is via the UPM. This is a workaround that may or may not work
		# for different Add-ons.
		find /tmp/plugins -type f -name "*.obr" -exec unzip -o -j -d /tmp/plugins {} \;
		find /tmp/plugins -type f -name "*.jar" -exec mv {} ${JIRA_HOME}/plugins/installed-plugins/ \;
		rm -rf /tmp/plugins
	fi
	# Reset the plugin cache, otherwise JIRA may not start
	if [ -d ${JIRA_HOME}/plugins/.osgi-plugins ]; then
		rm -r ${JIRA_HOME}/plugins/.osgi-plugins
	fi
	if [ -d ${JIRA_HOME}/plugins/.bundled-plugins ]; then
		rm -r ${JIRA_HOME}/plugins/.bundled-plugins
	fi
	rm -f /tmp/plugins
	touch ${JIRA_HOME}/plugins_installed
fi

if [ ! -f ${JIRA_INSTALL}/.configured ]; then
	touch ${JIRA_INSTALL}/.configured
else
	${JIRA_INSTALL}/bin/stop-jira.sh
fi

${JIRA_INSTALL}/bin/start-jira.sh
tail -Fn2000 ${JIRA_INSTALL}/logs/catalina.out
